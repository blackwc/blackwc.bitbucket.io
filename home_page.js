function show(obj) {
	var app_name = obj.title;
	var foo = document.getElementById("app_name");
	var bar = document.getElementById("app_desc");
	foo.innerHTML = app_name;
	bar.innerHTML = obj.childNodes[0].alt;
	
	foo.style.visibility = "visible";
	bar.style.visibility = "visible";
}

function mout() {

	var foo = document.getElementById("app_desc");
	var bar = document.getElementById("app_name");
	
	foo.style.visibility = "hidden";
	bar.style.visibility = "hidden";
	
	foo.innerHTML = "Description";
	bar.innerHTML = "Name";
}
