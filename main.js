var stretchFactor = 10;

function unroll(tip_n, tip_p) {
	var tip = document.getElementById("tooltip");
	
	if(tip_n < 10) {

		tip_n++;
		if(tip_n < 10) tip_p++;

		tip.style.height = tip_n + "px";
		tip.style.padding = tip_p + "px";
	
		setTimeout(function() {unroll(tip_n, tip_p)},15);
	}
}

function tooltip(msg, obj) {
	var tip = document.getElementById("tooltip");
	var left = obj.offsetLeft;
	var bottom = obj.offsetHeight;
	
	var tip_p = 0;
	var tip_n = 0;
	
	tip.style.height = 0+'px';
	tip.style.padding = 0+'px';
	
	tip.style.top = bottom + 5 + "px";
	tip.innerHTML = msg;
	
	tip.style.visibility="invisible";
	tip.style.display="inline";
	

	if((left + tip.offsetWidth) >= document.body.offsetWidth) {
		tip.style.left = (obj.offsetLeft - tip.offsetWidth) + stretchFactor*2 +"px";
	} else {
		tip.style.left = left+"px";
	}
	
	
	tip.style.visibility="visible";
	
	unroll(tip_n, tip_p);
}

function untooltip() {
	var tip = document.getElementById("tooltip");
	tip.style.display = "none";
	tip.innerHTML = "";
}


window.onload = function() {

	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
	{ 
		var ieversion=new Number(RegExp.$1);
		if (ieversion<9) return;
	}

	var toolbar = document.getElementById("toolbar");
	
	var bar = {};

	for (var i=0; i < toolbar.childNodes.length; i++){
		var foo = {};
		if(toolbar.childNodes[i].nodeType == 1) {
			foo.href = toolbar.childNodes[i].href;
			foo.title = toolbar.childNodes[i].title;
			foo.name = toolbar.childNodes[i].innerHTML;
			foo._class = toolbar.childNodes[i].className;
			toolbar.removeChild(toolbar.childNodes[i]);
			
			bar[i] = foo;
		}
	}
	
	for (x in bar) {
		var title = bar[x].title;
		var href = bar[x].href;
		var name = bar[x].name;
		var _class = bar[x]._class;
		var _float = bar[x].cssFloat;
		
		var node = document.createElement("div");
		node.setAttribute("class",_class);
		node.setAttribute("onclick","window.location.href='"+href+"'");
		node.setAttribute("onmouseover","tooltip('"+title+"',this)");
		node.setAttribute("onmouseout","untooltip()");
		node.innerHTML = name;
		toolbar.appendChild(node);
	}
}