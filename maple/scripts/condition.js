maple.condition = function(varname, oper,if_value,variables) {

	this.oper = oper;
	this.variables = variables;
	this.varname = varname;
	this.if_value = if_value;
}
	
maple.condition.prototype._run = function() {

	var self = this.self();

	var condition = false;

	switch(self.oper) {
		case IF.EQUAL:
			if(eval(self.variables[self.varname]) == self.if_value)
				condition = true;
		break;
		case IF.EQUAL_NOT:
			if(this.variables[self.varname] != self.if_value)
				condition = true;
		break;
		case IF.GREATER:
			if(eval(self.variables[self.varname]) > self.if_value)
				condition = true;
		break;
		case IF.GREATER_EQUAL:
			if(eval(self.variables[self.varname]) >= self.if_value)
				condition = true;
		break;
		case IF.LESS:
			if(eval(self.variables[self.varname]) < self.if_value)
				condition = true;
		break;
		case IF.LESS_EQUAL:
			if(eval(self.variables[self.varname]) <= self.if_value)
				condition = true;
		break;
	}

	if(condition) self.run();
}

maple.condition.prototype.run = function() {}

maple.condition.prototype.self = function() {
	return this;
}