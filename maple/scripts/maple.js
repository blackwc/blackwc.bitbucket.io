maple.core = function(g) {


	this.property = {

		backgroundColor: "RGB(0,0,0)",
		view: null,
		virtual: null,
		origin: new maple.position(0,0),
		contextMenu: true,
		firstScene: "scene",
		canvas_id: "maple",
		debugLog: false

	};

	this.variables = {}
	this.context = null;
	this.running = true;
	this.core = this;
	this.scene = null;
	this.game = g;
	this.resources = {};
	this.resources.sounds = [];
	this.resources.graphics = [];
	this.objects = {};
	this.create();

}

maple.core.prototype.create = function() {
		
	var self = this.self();

	self.log("initialize engine");

	self.property.virtual = new maple.size(self.game.core.virtual[0],
		self.game.core.virtual[1]);
	self.property.view = new maple.size(self.game.core.view[0],
		self.game.core.view[1]);
	self.property.firstScene = self.game.core.firstScene;
	self.property.backgroundColor = self.game.core.backgroundColor;
	self.property.debugLog = self.game.core.debugLog;

	
	var gamediv = document.createElement("canvas");
	gamediv.setAttribute("width", self.property.view.width);
	gamediv.setAttribute("height", self.property.view.height);
	gamediv.setAttribute("id", self.property.canvas_id);
	
	if(self.property.contextMenu)
		gamediv.setAttribute("oncontextmenu", "return false");

	self.log("set canvas view width '"+self.property.view.width+
		"px' and height '"+self.property.view.height+"px'");
	
	gamediv.style.cssText = 
		document.getElementById(self.property.canvas_id).style.cssText;
	
	self.log("set '"+self.property.canvas_id+"' as canvas");
	document.getElementById(self.property.canvas_id).parentNode.replaceChild(
		gamediv,document.getElementById(self.property.canvas_id));
	
	self.context = document.getElementById(
		self.property.canvas_id).getContext("2d");
	
	self.resources = self.game.resources;
	self.log("mapping resources");

	self.log("loop");
	self.loop();
	
	self.scene = new maple.scene({
		name: self.property.firstScene
	}, this);
}
	
maple.core.prototype.loop = function() {

	var self = this.self();

	if (!self.context && self.running) return;

	if (self.scene) self.scene.loop();

	if(self.running) requestAnimFrame(function(){self.loop()});
}

	
maple.core.prototype.onkeydown = function(event) {

	var self = this.self();

	for (var x in self.objects) {
		if(self.objects[x]) self.objects[x]._onkeydown(event);
	}
}
	
maple.core.prototype.onkeyup = function(event) {

	var self = this.self();

	for (var x in self.objects) {
		if(self.objects[x]) self.objects[x]._onkeyup(event);
	}
}
	
maple.core.prototype.log = function(msg) {

	var self = this.self();

	if(!self.debugLog) return;
	
	console.log(msg);
}

maple.core.prototype.self = function() {
	return this;
}