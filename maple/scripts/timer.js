maple.timer = function(interval) {

	this.tick = 0;
	this.running = false;
	this.end = 0;
	this._end = 0;
	this.name = "";
	this.onDone_events = [];
	this.onStart_events = [];
	this.inLogic_events = [];

	this.interval = interval;
}

maple.timer.prototype.start = function() {

	var self = this.self();

	for(var i = 0; i < self.onStart_events.length; i++) {
		if (self.onStart_events[i] != null) self.onStart_events[i]();
	}

	self.running = true;
	self._end = self.end;
	self.inc();
}
	
maple.timer.prototype.stop = function() {
	var self = this.self();
	self.running = false;
	self._end = self.tick;
}
	
maple.timer.prototype.inLogic = function() {

	var self = this.self();

	for(var i = 0; i < self.inLogic_events.length; i++) {
		if (self.inLogic_events[i] != null) self.inLogic_events[i]();
	}
}
	
maple.timer.prototype.inc = function() {

	var self = this.self();

	if((self.end == 0 || self._end > self.tick) && self.running) {
		setTimeout(function(){self.inc()},self.interval);
	} else {
		self.stop();
		if(self._end <= self.tick) {
			for(var i = 0; i < self.onDone_events.length; i++) {
				if (self.onDone_events[i] != null) self.onDone_events[i]();
			}
		}
		self.tick = 0;
		return;
	}
	
	self.inLogic();
	self.tick++;
}

maple.timer.prototype.self = function() {
	return this;
}