maple.object = function(props, world, c) {

	this.world = world;
	this.core = c;

	this.property = {
		name: "",
		graphic: null,
		rects: [],
		frame: 0,
		size: null,
		zindex: 0,
		origin: new maple.position(0,0),
		visible: true,
	}

	for(var foo in props) {
		this.property[foo] = props[foo];
	}

	this.ticker = null;
	this.variables = {};
	//this.then = 0;
	this.keynum = [];
	this.timers = {};
	//this.delta_t = 0;
	this.onCondition_events = {};
	this.onLogic_events = [];
	this.onCreate_events = [];
	this.onCollision_events = [];
	this.onKeyDown_events = [];
	this.onKeyUp_events = [];
}
	
maple.object.prototype.create = function() {

	var self = this.self();
	
	for(var i = 0; i < self.onCreate_events.length; i++) {
		if (self.onCreate_events[i] != null) self.onCreate_events[i]();
	}

	for(var i = 0; i < self.property.rects.length; i++ ) {
		var fixDef = new b2FixtureDef;
		fixDef.density = 1.0;
		fixDef.friction = 0.8;
		fixDef.restitution = 0.1;
		fixDef.shape = new b2PolygonShape;
		fixDef.shape.SetAsBox(
		self.property.rects[i].size.width /35   /2,
		self.property.rects[i].size.height / 35   /2
		);

		self.property.origin.x = self.property.rects[i].size.width/2;
		self.property.origin.y = self.property.rects[i].size.height/2;

		var b2body = new b2BodyDef;

		if(!self.dynamic) 
			b2body.type = b2Body.b2_staticBody;
		else
			b2body.type = b2Body.b2_dynamicBody;

		b2body.position.x = self.property.rects[i].position.x / 35;
		b2body.position.y = self.property.rects[i].position.y / 35;
		self.property.rects[i].body =
			self.world.CreateBody(b2body).CreateFixture(fixDef).GetBody();
		self.property.rects[i].body.SetFixedRotation(true);
    }

}
	
maple.object.prototype.onLogic = function() {

	var self = this.self();

	for(var i = 0; i < self.property.rects.length; i++ ) {
		if(self.property.rects[i].body) {
			self.property.rects[i].position.x =
				self.property.rects[i].body.GetPosition().x* 35 - self.property.size.width;
			self.property.rects[i].position.y =
				self.property.rects[i].body.GetPosition().y* 35 - self.property.size.height;
		}
	}

	//if(self.ticker != null && self.ticker.running)
	//	self.delta_t = self.ticker.tick - self.then;

	for(var i = 0; i < self.onLogic_events.length; i++) {
		if (self.onLogic_events[i] != null) self.onLogic_events[i]();
	}
	
	for(var i = 0; i < self.onCollision_events.length; i++) {
		if (!self.onCollision_events[i].fired)
			if (self.onCollision_events[i] != null)
				self.onCollision_events[i]._inLogic();
	}
}
	
maple.object.prototype.render = function(context) {

	var self = this.self();

	for (var i = 0; i < self.property.rects.length; i++) {

		if(self.property.graphic && self.property.visible) {
			context.drawImage(self.property.graphic.img,
			self.property.frame*self.property.rects[i].size.width,0,
			self.property.rects[i].size.width,
			self.property.rects[i].size.height,

			self.property.rects[i].position.x + self.property.origin.x,
			self.property.rects[i].position.y + self.property.origin.y,

			self.property.rects[i].size.width,
			self.property.rects[i].size.height);
		}
	}
}

maple.object.prototype.link = function(event, action) {

	var self = this.self();

	var evt = null;
	var args = arguments[2].concat(arguments[3]);

	switch(action) {
		case DO.NOTHING:
		
			evt = function(){};
			
		break;
		case DO.ALERT:
			evt = function() {
				var message = args[0];
				alert(message);
			}
		break;
		case DO.SET_GRAVITY:
			evt = function() {
				//if (self._key_check(event,args))
				//	self.gravity = [args[0],args[1]];
			}
		break;
		case DO.FOLLOW: {
			evt = function() {
				if(self._key_check(event,args)) {
					//TODO
				}
			}
		}
		break;
		case DO.TRACE: {
			evt = function() {
				if(self._key_check(event,args)) {
					var msg = args[0];
					self.core.log("TRACE: "+eval("\"" + msg + "\""));
				}
			}
		}
		break;
		case DO.TRACE_OBJECT: { 
			evt = function() {
				if(self._key_check(event,args)) {
					var o = args[0];
					var out = "";
					for (var p in o) {
						out += eval("\"" + p + "\"") + ":" + eval(o[p]) + "|";
					}
					self.core.log("TRACE: "+out);
				}
			}
		}
		break;
		case DO.MOVE: {
			evt = function() {
				if (self._key_check(event,args)) {
					var direction = args[0];
					var rate = args[1];
					self.move(direction,rate);
				}
			}
		
		}
		break;
		case DO.TIMER: {
			if (args[0] in self.timers) {
				evt = function() {
					if (self._key_check(event,args)) {
						var name = args[0];
						if (!self.timers[name].running)
							self.timers[name].start();
					}
				}
			}
		}
		break;
		case DO.ANIMATION: {
			evt = function() {
				if (self._key_check(event,args)) {
					var start = args[0]
					var stop = args[1]
					self.animate(start,stop);
				}
			}
		}
		break;
		case DO.SET_FRAME: {
			evt = function() {
				if (self._key_check(event, args)) {
					var frm = args[0];
					self.property.frame = frm;
				}
			}
		}
		break;
		case DO.TIMER_STOP:
			
			if(args[0] in self.timers) {
				evt = function() {
					var name = args[0];
					if (self._key_check(event, args)) {
						if (self.timers[name].running) self.timers[name].stop();
					}
				}
			}
		
		break;
		case DO.SET_VARIABLE:
			evt = function() {
				var name = args[0];
				var value = args[1];
				if(self._key_check(event, args)) {
					self.variables[name] = value;
				}
			}
			
		break;
		case DO.IF:
			
			if(args[0] in self.onCondition_events) {
				evt = function() {
					var name = args[0];
					if (self._key_check(event,args)) {
						for(var i = 0;
							i < self.onCondition_events[name].length;
							i++) {
							if(self.onCondition_events[name] != null) {
								self.onCondition_events[name][i]._run();
							}
						}
					}
				}
			}
			
		break;
		case DO.SCENE:
		
			evt = function() {
				var scn = args[0];
				if(self._key_check(event,args)) {
					self.core.scene = new maple.scene(scn,self.core);
				}
			}
		
		break;
		case DO.SOUND:

			if(self.core.check_dict(args[0], self.core.resources.sounds)) {
				evt = function() {
					if(self._key_check(event,args)) {
						if(!self.core.mute) {
							self.core.resources.sounds[args[0]].play();
						}
					}
				}
			}
		
		break;
		
		//TODO
		case DO.TRANSLATE:
		
			evt = function() {
				if(self._key_check(event,args)) {
					self.core.originX += args[0];
					self.core.originY += args[1];
					self.core.context.translate(-1*args[0],-1*args[1]);
				}
			}
		
		break;
		
		case DO.ALERT:
			
			evt = function() {
			
				alert(args[0]);				
			}
			
		break;
	}
	
	switch(event) {
		case ON.LOGIC:
			self.onLogic_events.push(evt);
		return;
		case ON.KEYHOLD:
			self.onLogic_events.push(evt);
		return;
		case ON.KEYDOWN:
			var key = _event_action_map(args[args.length-1]);
			var km = new maple.keymap();
			km.keys.push(key);
			km.event = evt;
			self.onKeyDown_events.push(km);
		return;
		case ON.KEYUP:
			var key = _event_action_map(args[args.length-1]);
			var km = new maple.keymap();
			km.keys.push(key);
			km.event = evt;
			self.onKeyUp_events.push(km);
		return;
		case ON.TIMER:
			
			var name = args[args.length-3];
			var interval = args[args.length-2];
			var end = args[args.length-1];
			
			if(name in self.timers) {
				self.timers[name].inLogic_events.push(evt);
			} else {
				var tmr = new maple.timer(interval);
				tmr.name = name;
				tmr.end = end;
				tmr.inLogic_events.push(evt);
			
				self.timers[name] = tmr;
			}
			
		return;
		case ON.TIMER_DONE:
		
			var name = args[args.length-1];

			if(name in self.timers)
				self.timers[name].onDone_events.push(evt);
		
		return;
		case ON.TIMER_START:
		
			var name = args[args.length-1];
			
			if(name in self.timers)
				self.timers[name].onStart_events.push(evt);
		
		return;
		case ON.CREATE:
			self.onCreate_events.push(evt);
		return;
		case ON.IF:
			var varname = args[args.length-4];
			var oper = args[args.length-3];
			var if_value = args[args.length-2];
			var name = args[args.length-1];
			
			var cnd = new maple.condition(varname,_event_action_map(oper),
				if_value,self.variables);
			cnd.run = evt;
			
			if(!(name in self.onCondition_events))
				self.onCondition_events[name] = [];
			
			self.onCondition_events[name].push(cnd);
		return;
	}
}
	
maple.object.prototype._key_check = function(event,args) {

	var self = this.self();

	if (event == ON.KEYDOWN || event == ON.KEYHOLD) {
		if(self.keynum.length == 0) return false;
		if(self.keynum.indexOf(_event_action_map(args[args.length-1])) == -1)
			return false;
	}

	return true;
}
	
maple.object.prototype._onkeydown = function(event) {

	var self = this.self();

	var e = event;
	var key = e.KeyCode || e.which;
	if (self.keynum.indexOf(key) == -1) self.keynum.push(key);
	
	for(var i = 0; i < self.onKeyDown_events.length; i++) {
		if (self.onKeyDown_events[i].keys.indexOf(key) != -1) {
			
			if(!self.onKeyDown_events[i].fired) {
				if (self.onKeyDown_events[i] != null)
					self.onKeyDown_events[i].event();
			}
			
			self.onKeyDown_events[i].fired = true;
		}
	}
}

maple.object.prototype._onkeyup = function(event) {

	var self = this.self();

	var e = event;
	var key = e.KeyCode || e.which;
	var ret = self.keynum.indexOf(key);
	
	for(var i = 0; i < self.onKeyDown_events.length; i++) {
		if(self.onKeyDown_events[i].keys.indexOf(key) != -1) {
			if(self.onKeyDown_events[i].fired)
				if (self.onKeyDown_events[i] != null)
					self.onKeyDown_events[i].fired = false;
		}
	}
	
	for(var i = 0; i < self.onKeyUp_events.length; i++) {
		if(self.onKeyUp_events[i].keys.indexOf(key) != -1) {
			if (self.onKeyUp_events[i] != null)
				self.onKeyUp_events[i].event();
		}
	}
	
	if (ret != -1) self.keynum.splice(ret,1);
}
	
maple.object.prototype.move = function(direction,velocity) {

	var self = this.self();

	direction = -1*direction;

	var xmod = (velocity)*(Math.cos((Math.PI/180)*direction)) / 35;
	var ymod = (velocity)*(Math.sin((Math.PI/180)*direction)) / 35;

	for(var i = 0; i < self.property.rects.length; i++ ) {
		if(self.property.rects[i].body)
			self.property.rects[i].body.ApplyImpulse({ x: xmod, y: ymod },
				self.property.rects[i].body.GetWorldCenter());
	}

	/*

	//TODO
	if(self.tracked[0]) {

		if ((self.core.originX + xmod) >= 0 && (self.core.originX + xmod) <= (self.core.w - self.core.view_w)) {
			
			var rect = new maple.rect(self.core.originX + self.core.view_w/2,0,100,self.core.view_h);
			//var rect = new Rect(0,0,self.core.w,self.core.h).deflate((self.core.view_w - self.w)/2);
			//if (rect.contains(self.x + self.w/2,rect.y)) {
			if (rect.contains(self.x + self.w/2,rect.y)) {
				self.core.context.translate(-1*xmod,0);
				self.core.originX += xmod;

				//self.core.Debug(self.x + ' ' + Number(rect.x+rect.w));
			}
		}
	}
	*/

	/*

	if(self.tracked[1]) {

		if ((self.core.originY + ymod) >= 0 && (self.core.originY + ymod) <= (self.core.h - self.core.view_h)) {
			

			var rect = new Rect(0,0,self.core.w,self.core.h).deflate((self.core.view_h - self.h)/2);
			if (rect.contains(rect.x,self.y + self.h/2)) {
				self.core.context.translate(0, -1*ymod*.1);
				self.core.originY += ymod*.1;

				//self.core.Debug(self.x + ' ' + Number(rect.x+rect.w));
			}
		}
	}
	*/
	
}
	
maple.object.prototype.animate = function(start,stop) {

	var self = this.self();

	if(self.property.frame < start || self.property.frame > stop) self.property.frame = start;
	if (self.property.frame >= stop) self.property.frame = start-1;
	self.property.frame++;
}

maple.object.prototype.self = function() {
	return this;
}