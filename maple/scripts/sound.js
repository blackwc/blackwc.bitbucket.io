maple.sound = function(snd) {
	var is_ie = false;
	
	if (navigator.appVersion.indexOf("Trident") != -1 ||
	navigator.appVersion.indexOf("MSIE") != -1) is_ie = true;
	
	var ext = snd.split(".");
	var ext1 = ext[ext.length];
	
	if (is_ie && ext1 != "mp3") {
		ext[ext.length-1] = "mp3";
		ext = ext.join(".");
		snd = ext;
	}
	
	this.snd = new Audio(snd);
}

maple.sound.prototype.play = function() {
	var self = this.self();
	self.snd.play();
}

maple.sound.prototype.self = function() {
	return this;
}