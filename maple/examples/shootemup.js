var game =
{
	"core": {
		"backgroundcolor": "RGB(224,243,240)",
		"virtual": [320,280],
		"view": [320,280],
		"firstScene": "scene1",
		"debugLog": false
	},
	"resources": {
		//"sounds": {
		//	"explode": "res/explosion.ogg"
		//},
		"graphics": {
			"stars_bg": "res/stars_bg.png",
			"enemy_img": "res/enemy.png",
			"explosion_img": "res/explosion.gif",
			"ship_img": "res/ship.png"
		}
	},
	"objects": {
		"explosion": {
			"graphic": "explosion_img",
			"map": [
				{
					"ON.TIMER": [
						[
							["anim", 100, 12],
							["DO.ANIMATION", 6, 16]
						]
					]
				},
				{
					"ON.CREATE": [
						[
							[0],
							["DO.TIMER", "anim"]//,
							//["DO.SOUND", "explode"]
						]
					]
				}
			]
		},
		"enemy": {
			"graphic": "enemy_img",
			"dynamic": true,
			"map": [
				{
					"ON.LOGIC": [
						[
							[0],
							["DO.MOVE", 270, 0.3]
						]
					]
				}
			]
		},
		"ship": {
			"graphic": "ship_img",
			"dynamic": true,
			"map": [
				{
					"ON.KEYHOLD": [
						[
							["KEY.W"],
							["DO.MOVE", 90, 1]
						],
						[
							["KEY.D"],
							["DO.MOVE", 0 , 1]
						],
						[
							["KEY.A"],
							["DO.MOVE", 180, 1]
						],
						[
							["KEY.S"],
							["DO.MOVE", 270, 1]
						]
					]
				}
			]
		}	
	},
	"scenes": {
		"scene1": {
			"origin": [0,0],
			"graphics": {
				"stars_bg": [
					{
						"z": 0,
						"pos": [[0,0]],
						"tile": [0,0,320,280]
					}
				]
			},
			"objects": [
				{
					"name": "ship",
					"pos": [[148,237]],
					"size": [24,24],
					"z": 1
				},
				{
					"name": "enemy",
					"pos": [[100,-20]],
					"size": [24,24],
					"z": 1
				},
				{
					"name": "explosion",
					"size": [32,45],
					"pos": [[10,10]],
					"z": 2
				}
			]
		}
	}
}