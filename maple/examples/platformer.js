var game =
{
	"core": {
		"virtual": [640,1000],
		"view": [320,280],
		"firstScene": "scene1",
		//"backgroundColor": "RGB(0,0,0)",
		//"backgroundColor": "RGB(175,226,239)",
		"backgroundColor": "RGB(22,90,107)",
		"debugLog": false
	},
	"resources": {
		//"sounds": {
		//	"jump": "res/jump.ogg"
		//},
		"graphics": {
			"sprites_img": "res/sprites.png",
			"plant_img": "res/plant.gif",
			"turtle_img": "res/turtle.gif",
			"mario_img": "res/mario.gif"

		}
	},
	"objects": {
		"solid": {},
		//"solid1": {},
		"solid2": {},
		"solid3": {},
		"turtle": {
			"graphic": "turtle_img",
			"dynamic": true,
			"map": [
				{
					"ON.TIMER": [
						[
							["walk_left", 150, 60],
							["DO.ANIMATION", 2,3],
							["DO.MOVE", 180, 10]
						],
						[
							["walk_right", 150, 60],
							["DO.ANIMATION", 0,1],
							["DO.MOVE", 0, 10]
						]
					]
				},
				{
					"ON.TIMER_DONE": [
						[
							["walk_left"],
							["DO.TIMER", "walk_right"]
						],
						[
							["walk_right"],
							["DO.TIMER", "walk_left"]
						]
					]
				},
				{
					"ON.CREATE": [
						[
							[0],
							["DO.TIMER", "walk_left"]
						]
					]
				}
			]
		},
		"plant": {
			"graphic": "plant_img",
			"map": [
				{
					"ON.TIMER": [
						//[
						//	["up_timer", 66, 34],
						//	["DO.MOVE", 90, 1]
						//],
						//[
						//	["down_timer", 66, 34],
						//	["DO.MOVE", 270, 1]
						//],
						//[
						//	["wait_timer",1000,2],
						//	["DO.NOTHING", 0]
						//],
						//[
						//	["wait_timer1", 550, 8],
						//	["DO.ANIMATION", 0, 1]
						//]
						[
							["chomp_timer", 500, 0],
							["DO.ANIMATION", 0, 1]
						]
					]
				},
				/*{
					"ON.TIMER_DONE": [
						[
							["up_timer"],
							["DO.TIMER", "wait_timer1"]
						],
						[
							["down_timer"],
							["DO.TIMER", "wait_timer"]
						],
						[
							["wait_timer"],
							["DO.TIMER", "up_timer"]
						],
						[
							["wait_timer1"],
							["DO.TIMER", "down_timer"]
						]
					]
				},*/
				{
					"ON.CREATE": [
						[
							[0],
							//["DO.TIMER", "wait_timer1"]
							["DO.TIMER","chomp_timer"]
						]
					]
				}
			]
		},
		"mario": {
			"graphic": "mario_img",
			"dynamic": true,
			"map": [
				{
					"ON.CREATE": [
						[
							[0],
							["DO.SET_VARIABLE", "jumping", false]
						]
					]
				},
				//{

				//	"ON.LOGIC": [
				//		[
				//			[0],
							//["DO.SET_GRAVITY", 270, 3]
				//		]
				//	]
				//},
				{
					"ON.TIMER": [
						[
							["up_timer",15,20],
							//["DO.SET_GRAVITY", 270, 0],
							["DO.MOVE", 90, 5],
							["DO.SET_VARIABLE", "jumping", true]
							//["DO.SOUND", "jump"]
						],
						[
							["ra_timer",100,0],
							["DO.ANIMATION", 0, 2]
						],
						[
							["la_timer",100,0],
							["DO.ANIMATION", 3, 5]
						]
					]
				},
				{
					"ON.TIMER_DONE": [
						[
							["up_timer"],
							["DO.SET_VARIABLE","jumping",false]
						]
					]
				},
				{
					"ON.IF": [
						[
							["jumping","IF.EQUAL",false,"if_jump"],
							["DO.TIMER", "up_timer"]
						]
					]
				},
				{
					"ON.KEYHOLD": [
						[
							["KEY.A"],
							["DO.MOVE", 180, 2.5]
						],
						[
							["KEY.D"],
							["DO.MOVE", 0, 2.5]
						]
					]
				},
				{
					"ON.KEYDOWN": [
						[
							["KEY.W"],
							["DO.IF", "if_jump"]
						],
						[
							["KEY.A"],
							["DO.TIMER", "la_timer"]
						],
						[
							["KEY.D"],
							["DO.TIMER", "ra_timer"]
						]
					]
				},
				{
					"ON.KEYUP": [
						[
							["KEY.D"],
							["DO.TIMER_STOP", "ra_timer"],
							["DO.SET_FRAME", 0]
						],
						[
							["KEY.A"],
							["DO.TIMER_STOP", "la_timer"],
							["DO.SET_FRAME", 3]
						]
					]
				}
			]
		}
	},
	"scenes": {
		"scene1": {
			"graphics": {
				"sprites_img": [
					{ 	"name": "ground",
						"z": 1,
						"pos": [[0,463],[17,463],[34,463],[51,463],[68,463],
						[85,463],[102,463],[119,463],[136,463],[153,463],
						[170,463],[187,463],[204,463],[221,463],[238,463],
						[255,463],[272,463],[289,463],[306,463],[323,463],
						[340,463],[357,463],[374,463],[391,463],[408,463],
						[425,463],[442,463],[459,463],[476,463],[493,463],
						[510,463],[561,463],[578,463]],
						"tile": [8,415,17,17]
					},
					{
						"name":"mounds",
						"z": 1,
						"pos": [[10,399]],
						"tile": [155,1,96,65]
					},
					//{
					//	"name":"blocks",
					//	"z": 1,
					//	"pos": [[110,415]],
					//	"tile": [196,340,32,16]
					//},
					{
						"name":"clouds",
						"z": 2,
						"pos": [[180,325],[70,250]],
						"tile": [5,318,64,23]
					},
					{
						"name":"tunnel",
						"z": 3,
						"pos": [[260,415]],
						"tile": [194,229,32,48]
					},
					{
						"name":"bushes",
						"z": 4,
						"pos": [[200,447]],
						"tile": [23,44,48,16]
					}
				]
			},
			"objects": [
				{
					"name": "solid",
					"z": 1,
					"pos": [[0,463]],
					"size": [320,17]
				},
				//{
				//	"name":"solid1",
				//	"z": 1,
				//	"pos": ["blocks"],
				//	"size": [32,16]
				//},
				{
					"name":"solid2",
					"z": 1,
					"pos": [[0, 344]],
					"size": [17,119]
				},
				{
					"name":"solid3",
					"z": 1,
					"pos":["tunnel"],
					"size": [32,48]
				},
				{
					"name":"turtle",
					"z": 5,
					"size": [16,27],
					"pos": [[100,436]]
				},
				{
					"name":"plant",
					"z": 2,
					"pos": [[264,385]],
					"size": [24,33]
				},
				{
					"name": "mario",
					"z": 3,
					"pos":[[160,320]],
					"size": [16,28]
				}
			],
			"origin": [0,200],
			"gravity": [0,10]
		}
	}
}